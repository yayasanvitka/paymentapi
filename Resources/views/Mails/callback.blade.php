@component('mail::message')
<h3>Callback Error Has Occurred</h3>

@component('mail::table')
| Error Code        | Message              |
|:------------------|----------------------|
| {{ $data['code'] }} | {{ $data['message'] }} |
@endcomponent

@endcomponent
