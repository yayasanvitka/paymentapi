<?php

namespace Modules\Paymentapi\Traits;

use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Modules\PaymentApi\Http\Controllers\BniEnc;

/**
 * Trait ConnectorTrait.
 *
 * @package Modules\Paymentapi\Traits
 */
trait ConnectorTrait
{
    /**
     * @var string
     */
    public $encryptedData;

    /**
     * @return $this
     */
    public function encrypt()
    {
        $this->encryptedData = BniEnc::encrypt($this->data, $this->clientId, $this->clientKey);

        return $this;
    }

    /**
     * Send to BNI API.
     */
    public function sendToBankApi(): ?Collection
    {
        $client = new Client();

        $response = $client->request(
            'POST',
            config('paymentapi.client.url'),
            [
                'headers' => ['Accept-Encoding' => 'gzip'],
                'json' => [
                    'client_id' => $this->clientId,
                    'data' => $this->encryptedData,
                ],
            ]
        );

        return $this->makeResponse($response);
    }
    
    /**
     * @param $response
     * @return Collection|null
     */
    public function makeResponse($response): ?Collection
    {
        if (200 == $response->getStatusCode()) {
            $data = json_decode($response->getBody(), true);
            if ('000' == $data['status']) {
                $data['data'] = BniEnc::decrypt($data['data'], $this->clientId, $this->clientKey);
            }

            return collect($data);
        } else {
            return collect([
                'status' => '500',
                'message' => 'Internal Server Error',
            ]);
        }
    }
}
