## About This Package
This package provides easy integration between BNI API and Laravel Apps.

## Installation
Currently, **there is no easy way to install it to your development environment, especially if you want to develop this package further!**

To Yayasan Vitka's Developers, please pull this package somewhere, and add this line to your composer.json:
```
"repositories": [
        {
            "type": "path",
            "url": "C:\\PATH\\TO\\MODULE\\FOLDER",
            "packagist.org": false,
            "options": {
                "symlink": true
            }
        }
    ]
```
Then, you can add to require:
```
"yayasanvitka/paymentapi-module": "dev-master as 1.0"
```

If you are using yayasanvitka/siakad, the module will be at
```
Modules/Paymentapi
```

## License
yayasanvitka/paymentapi is licensed under GPL-3.0-or-later license