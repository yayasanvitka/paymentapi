<?php

namespace Modules\PaymentApi\Providers;

use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Modules\PaymentApi\PaymentApi;

/**
 * Class PaymentApiServiceProvider.
 *
 * @package Modules\PaymentApi\Providers
 */
class PaymentApiServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerViews();
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerConfig();
        $this->app->register(RouteServiceProvider::class);
        $this->app->bind('PaymentApi', function () {
            return new PaymentApi();
        });
    }

    /**
     * Override Config if value exists on database.
     *
     * @return void
     */
    protected function overrideConfigValues()
    {
        if (Schema::hasTable('settings')) {
            $data = DB::table('settings')->where('key', 'LIKE', 'paymentApi%')->get();

            foreach ($data as $d) {
                if (isset($d->value) && !empty($d->value)) {
                    config([$d->key => $d->value]);
                }
            }
        }
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('paymentapi.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php',
            'paymentapi'
        );

        // load this after default config is loaded to ensure .env is loaded too
        $this->overrideConfigValues();
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/paymentapi');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath,
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path.'/modules/paymentapi';
        }, \Config::get('view.paths')), [$sourcePath]), 'paymentapi');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/paymentapi');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'paymentapi');
        } else {
            $this->loadTranslationsFrom(__DIR__.'/../Resources/lang', 'paymentapi');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production')) {
            app(Factory::class)->load(__DIR__.'/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
