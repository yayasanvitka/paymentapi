<?php

namespace Modules\Paymentapi\Transactions;

use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Modules\Paymentapi\Traits\ConnectorTrait;

/**
 * Class TransactionAbstract.
 *
 * @package Modules\Paymentapi\Transactions
 */
abstract class TransactionAbstract implements TransactionInterface
{
    use ConnectorTrait;

    const VA_PREFIX = 988;

    public $clientId;
    public $clientKey;
    public $va;
    public $expiredOn;

    public $data;
    
    /**
     * @param string $vaNum
     * @return $this
     */
    public function setVA(string $vaNum)
    {
        $this->va = self::VA_PREFIX.$this->clientId.$vaNum;

        return $this->va;
    }
    
    /**
     * Set Client ID and Key.
     * @param array $data
     */
    public function setConfig(array $data)
    {
        $this->clientId = $data['client_id'];
        $this->clientKey = $data['client_key'];
    }
    
    /**
     * @param array $data
     * @return $this
     */
    public function prepare(array $data)
    {
        $this->setConfig($data);

        return $this;
    }

    /**
     * @return $this|RedirectResponse
     */
    public function validate()
    {
        $validator = Validator::make($this->data, [
            'client_id' => 'required|digits:3',
            'virtual_account' => 'required|digits:16',
        ]);

        if ($validator->fails()) {
            return $validator->errors();
        }

        return $this;
    }

    /**
     * Send data to BNI API.
     *
     * @return Collection|null
     */
    public function send()
    {
        return $this->encrypt()->sendToBankApi();
    }

    /**
     * @return $this
     */
    public function generateBillingLifetime()
    {
        $this->expiredOn = now()
            ->add(config('paymentapi.va.default_lifetime'), 'day')
            ->locale('id')
            ->isoFormat('YYYY-MM-DDTHH:mm:ss'.'+07:00');

        return $this->expiredOn;
    }
}
