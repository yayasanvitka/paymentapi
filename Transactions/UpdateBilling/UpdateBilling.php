<?php

namespace Modules\Paymentapi\Transactions\UpdateBilling;

use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Modules\Paymentapi\Transactions\TransactionAbstract;

/**
 * Class UpdateBilling.
 *
 * @package Modules\Paymentapi\Transactions\UpdateBilling
 */
class UpdateBilling extends TransactionAbstract
{
    /**
     * @var string
     */
    public $type = 'updatebilling';
    /**
     * @var string
     */
    public $billing_type = 'i';
    /**
     * @var array
     */
    public $data;
    
    /**
     * @param array $data
     * @return $this|UpdateBilling
     */
    public function prepare(array $data)
    {
        $this->setConfig($data);

        $this->data = [
            'client_id' => $this->clientId,
            'trx_id' => $data['transactionId'],
            'trx_amount' => $data['amount'],
            'customer_name' => $data['cust_name'], //optional
            'customer_email' => $data['cust_email'], //optional
            'customer_phone' => $data['cust_phone'], //optional
            'datetime_expired' => (isset($data['datetime_expired']) ? $data['datetime_expired'] : $this->generateBillingLifetime()),
            'description' => $data['description'],
            'type' => $this->type,
        ];

        return $this;
    }
    
    /**
     * @param array $data
     * @return $this
     */
    public function expiring(array $data)
    {
        $this->setConfig($data);

        $this->expiredOn = now()
            ->locale('id')
            ->isoFormat('YYYY-MM-DDTHH:mm:ss'.'+07:00');

        $this->data = [
            'client_id' => $this->clientId,
            'trx_id' => $data['transactionId'],
            'trx_amount' => $data['amount'],
            'customer_name' => $data['cust_name'], //optional
            'customer_email' => $data['cust_email'], //optional
            'customer_phone' => $data['cust_phone'], //optional
            'datetime_expired' => $this->expiredOn,
            'description' => $data['description'],
            'type' => $this->type,
        ];

        return $this;
    }

    /**
     * @return $this|RedirectResponse|UpdateBilling
     */
    public function validate()
    {
        $validator = Validator::make($this->data, [
            'client_id' => 'required|digits:5',
            'trx_id' => 'required|string',
            'trx_amount' => 'required',
            'customer_name' => 'required|string',
            'customer_email' => 'email|nullable',
            'customer_phone' => 'numeric|nullable',
            'datetime_expired' => 'required|date',
            'description' => 'string',
            'type' => [
                'required',
                Rule::in(['updatebilling']),
            ],
        ]);

        if ($validator->fails()) {
            abort(500, $validator->errors());
        }

        return $this;
    }
}
