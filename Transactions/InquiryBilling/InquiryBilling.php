<?php

namespace Modules\Paymentapi\Transactions\InquiryBilling;

use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Modules\Paymentapi\Transactions\TransactionAbstract;

/**
 * Class InquiryBilling.
 *
 * @package Modules\Paymentapi\Transactions\InquiryBilling
 */
class InquiryBilling extends TransactionAbstract
{
    public $type = 'inquirybilling';
    public $billing_type = 'i';
    public $data;

    public function prepare(array $data)
    {
        $this->setConfig($data);

        $this->data = [
            'client_id' => $this->clientId,
            'trx_id' => $data['transactionId'],
            'type' => $this->type,
        ];

        return $this;
    }

    /**
     * @return $this|RedirectResponse|InquiryBilling
     */
    public function validate()
    {
        $validator = Validator::make($this->data, [
            'client_id' => 'required|digits:5',
            'trx_id' => 'required|string',
            'type' => [
                'required',
                Rule::in(['inquirybilling']),
            ],
        ]);

        if ($validator->fails()) {
            abort(500, $validator->errors());
        }

        return $this;
    }
}
