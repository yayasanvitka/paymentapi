<?php

namespace Modules\Paymentapi\Transactions;

/**
 * Interface TransactionInterface.
 *
 * @package Modules\Paymentapi\Transactions
 */
interface TransactionInterface
{
    /**
     * @param string $vaNum
     * @return mixed
     */
    public function setVA(string $vaNum);
    
    /**
     * @param array $data
     * @return mixed
     */
    public function prepare(array $data);

    /**
     * @return mixed
     */
    public function validate();

    /**
     * @return mixed
     */
    public function send();

    /**
     * @return mixed
     */
    public function generateBillingLifetime();
}
