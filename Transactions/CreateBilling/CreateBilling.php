<?php

namespace Modules\Paymentapi\Transactions\CreateBilling;

use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Modules\Paymentapi\Transactions\TransactionAbstract;

/**
 * Class CreateBilling.
 *
 * @package Modules\Paymentapi\Transactions\CreateBilling
 */
class CreateBilling extends TransactionAbstract
{
    public $type = 'createbilling';
    public $billing_type = 'i';
    public $data;
    
    /**
     * @param array $data
     * @return $this|CreateBilling
     */
    public function prepare(array $data)
    {
        $this->setConfig($data);

        $this->data = [
            'client_id' => $this->clientId,
            'virtual_account' => $data['vaNum'],
            'trx_id' => $data['transactionId'],
            'trx_amount' => $data['amount'],
            'customer_name' => $data['cust_name'], //optional
            'customer_email' => $data['cust_email'], //optional
            'customer_phone' => $data['cust_phone'], //optional
            'datetime_expired' => (isset($data['datetime_expired']) ? $data['datetime_expired'] : $this->generateBillingLifetime()),
            'description' => $data['description'],
            'type' => $this->type,
            'billing_type' => $this->billing_type,
        ];

        return $this;
    }

    /**
     * @return $this|RedirectResponse|CreateBilling
     */
    public function validate()
    {
        $validator = Validator::make($this->data, [
            'client_id' => 'required|digits:5',
            'virtual_account' => 'required|digits:16',
            'trx_id' => 'required|string',
            'trx_amount' => 'required|digits_between:5,10',
            'customer_name' => 'required|string',
            'customer_email' => 'email|nullable',
            'customer_phone' => 'numeric|nullable',
            'datetime_expired' => 'required|date',
            'description' => 'string',
            'type' => [
                'required',
                Rule::in(['createbilling']),
            ],
            'billing_type' => [
                'required',
                Rule::in(['c', 'i']),
            ],
        ]);

        if ($validator->fails()) {
            abort(500, $validator->errors());
        }

        return $this;
    }
}
