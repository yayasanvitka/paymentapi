<?php

return [
    'name' => 'PaymentApi',

    'valid_types' => [
        'createbilling',
        'inquirybilling',
        'updatebilling',
    ],

    'billing_types' => [
        'c' => 'fixed',
        'i' => 'installment',
    ],

    /*
     * Status Message returned from API
     */
    'status' => [
        '000' => 'Success',
        '001' => 'Incomplete / Invalid Parameter',
        '002' => 'IP address not found or wrong Client ID',
        '004' => 'Service Not Found',
        '005' => 'Service Not Defined',
        '006' => 'Invalid VA Number',
        '008' => 'Technical Failure',
        '009' => 'Unexpected Error',
        '010' => 'Request Timeout',
        '011' => 'Billing type does not match billing amount',
        '012' => 'Invalid expiry date / time',
        '013' => 'IDR Currency cannot have billing with decimal fraction',
        '101' => 'Billing ID not found',
        '102' => 'VA Number is in use',
        '103' => 'Billing has been expired',
        '105' => 'Duplicate Billing ID',
        '107' => 'Amount can not be changed',
        '200' => 'Failed to send SMS Payment',
        '201' => 'SMS Payment can only be used with Fixed Payment',
        '997' => 'System is temporarily offline',
        '998' => '"Content-Type" header not defined as it should be',
        '999' => 'Internal Error',
    ],

    'client' => [
        'id' => env('PAYMENTAPI_CLIENT_ID'),
        'key' => env('PAYMENTAPI_CLIENT_KEY'),
        'url' => env('PAYMENTAPI_URL'),
    ],

    'va' => [
        'default_lifetime' => '60', //lifetime in day,
    ],

    'notification' => [
        'mailer' => [
            'host' => env('MAIL_HOST'),
            'port' => env('MAIL_PORT'),
            'username' => env('MAIL_USERNAME'),
            'name' => env('MAIL_FROM_NAME'),
            'address' => env('MAIL_FROM_ADDRESS'),
            'password' => env('MAIL_PASSWORD'),
            'encryption' => env('MAIL_ENCRYPTION'),
        ],
        'callback' => [
            'errors' => 1,
        ],
        'notify_to' => 'a@a.com',
    ],
];
