<?php

namespace Modules\PaymentApi\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class PaymentApiConfigTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->settingList() as $index => $setting) {
            $result = \DB::table('settings')->insert($setting);

            if (!$result) {
                $this->command->info("Insert failed at record $index.");

                return;
            }
        }
    }

    private function settingList()
    {
        return [
            [
                'key' => 'paymentapi.client.id',
                'name' => 'ClientID from BNI',
                'description' => 'ClientID to Authenticate to server',
                'value' => env('PAYMENTAPI_CLIENT_ID') ? env('PAYMENTAPI_CLIENT_ID') : '',
                'field' => '{"name":"value","label":"Value","type":"text"}',
                'active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'key' => 'paymentapi.client.key',
                'name' => 'ClientKey from BNI',
                'description' => 'ClientKey to Authenticate to server',
                'value' => env('PAYMENTAPI_CLIENT_KEY') ? env('PAYMENTAPI_CLIENT_KEY') : '',
                'field' => '{"name":"value","label":"Value","type":"text"}',
                'active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'key' => 'paymentapi.client.url',
                'name' => 'API URL',
                'description' => 'URL to server',
                'value' => env('PAYMENTAPI_URL') ? env('PAYMENTAPI_URL') : '',
                'field' => '{"name":"value","label":"Value","type":"text"}',
                'active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'key' => 'paymentapi.callback.url',
                'name' => 'API Callback URL',
                'description' => 'Callback URL for Payment Notification',
                'value' => 'v1/payment/notifier',
                'field' => '{"name":"value","label":"Value","type":"text"}',
                'active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'key' => 'paymentapi.notification.callback.errors',
                'name' => 'Mail Notification Callback Error',
                'description' => 'Send mail when Callback Error Occurred?',
                'value' => 1,
                'field' => '{"name":"value","label":"Value","type":"checkbox"}',
                'active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'key' => 'paymentapi.va.default_lifetime',
                'name' => 'VA Lifetime (days)',
                'description' => 'Maximum VA Lifetime when creating',
                'value' => 60,
                'field' => '{"name":"value","label":"Value","type":"number"}',
                'active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'key' => 'paymentapi.notification.mailer.host',
                'name' => 'PaymentApi Mailer host',
                'description' => '',
                'value' => env('MAIL_HOST'),
                'field' => '{"name":"value","label":"Value","type":"text"}',
                'active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'key' => 'paymentapi.notification.mailer.port',
                'name' => 'PaymentApi Mailer port',
                'description' => '',
                'value' => env('MAIL_PORT'),
                'field' => '{"name":"value","label":"Value","type":"text"}',
                'active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'key' => 'paymentapi.notification.mailer.username',
                'name' => 'PaymentApi Mailer username',
                'description' => '',
                'value' => env('MAIL_USERNAME'),
                'field' => '{"name":"value","label":"Value","type":"text"}',
                'active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'key' => 'paymentapi.notification.mailer.name',
                'name' => 'PaymentApi Mailer name',
                'description' => '',
                'value' => env('MAIL_FROM_NAME'),
                'field' => '{"name":"value","label":"Value","type":"text"}',
                'active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'key' => 'paymentapi.notification.mailer.address',
                'name' => 'PaymentApi Mailer address',
                'description' => '',
                'value' => env('MAIL_FROM_ADDRESS'),
                'field' => '{"name":"value","label":"Value","type":"text"}',
                'active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'key' => 'paymentapi.notification.mailer.password',
                'name' => 'PaymentApi Mailer password',
                'description' => '',
                'value' => env('MAIL_PASSWORD'),
                'field' => '{"name":"value","label":"Value","type":"password"}',
                'active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'key' => 'paymentapi.notification.mailer.encryption',
                'name' => 'PaymentApi Mailer encryption',
                'description' => '',
                'value' => env('MAIL_ENCRYPTION'),
                'field' => '{"name":"value","label":"Value","type":"select2_from_array","options":{"":"None","SSL":"SSL","TLS":"TLS"}}',
                'active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'key' => 'paymentapi.notification.notify_to',
                'name' => 'Send Notification To',
                'description' => '',
                'value' => 'a@a.com',
                'field' => '{"name":"value","label":"Value","type":"text"}',
                'active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ];
    }
}
