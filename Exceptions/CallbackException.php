<?php

namespace Modules\Paymentapi\Exceptions;

use Carbon\Carbon;
use Exception;
use Modules\PaymentApi\Jobs\CallbackNotificationEmailJob;
use Throwable;

/**
 * Class CallbackException.
 *
 * @package Modules\Paymentapi\Exceptions
 */
class CallbackException extends Exception
{
    protected $message;
    protected $code;
    protected $subject;

    /**
     * CallbackException constructor.
     *
     * @param string $message
     * @param int $code
     * @param null $subject
     */
    public function __construct($message = '', $code = 0, $subject = null, Throwable $previous = null)
    {
        $this->subject = $subject;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return mixed
     */
    public function render()
    {
        return response()->json([
            'status' => "$this->code",
            'message' => $this->message,
        ], 500);
    }

    /**
     * Send Error report.
     */
    public function report()
    {
        if (config('paymentapi.notification.callback.errors')) {
            $data = [];
            if (isset($this->subject) && null != $this->subject) {
                $data['subject'] = $this->subject;
            }

            $data['message'] = $this->message;
            $data['code'] = $this->code;

            $job = (new CallbackNotificationEmailJob($data))->delay(Carbon::now()->addSeconds(5));
            dispatch($job);
        }
    }
}
