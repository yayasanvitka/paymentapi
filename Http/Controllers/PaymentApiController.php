<?php

namespace Modules\PaymentApi\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Paymentapi\Exceptions\CallbackException;

/**
 * Class PaymentApiController.
 *
 * @package Modules\PaymentApi\Http\Controllers
 */
class PaymentApiController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function respond(Request $request)
    {
        $this->validate($request);

        $data = BniEnc::decrypt($request->data, config('paymentapi.client.id'), config('paymentapi.client.key'));

        throw_unless($data, new CallbackException('Incorrect Time / Incorrect Key!', '999', '[Callback Error: 999]'));

        return response()->json([
            'status' => '000',
        ], 200);
    }

    public function validate(Request $request): bool
    {
        throw_unless($request->isJson(), new CallbackException('Invalid Request', '999', '[Callback Error: 999]'));

        throw_unless(
            ($request->client_id == config('paymentapi.client.id')),
            new CallbackException('Invalid Client ID!', '999', '[Callback Error: 999]')
        );

        throw_unless(
            $request->has('data'),
            new CallbackException('Empty Data is Not Allowed!', '999', '[Callback Error: 999]')
        );

        return true;
    }
}
