<?php

namespace Modules\PaymentApi\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Modules\PaymentApi\Emails\CallbackNotificationEmail;

/**
 * Class CallbackNotificationEmailJob.
 *
 * @package Modules\PaymentApi\Jobs
 */
class CallbackNotificationEmailJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $data;
    
    /**
     * Create a new job instance.
     * CallbackNotificationEmailJob constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to(config('paymentapi.notification.notify_to'))
            ->send(new CallbackNotificationEmail($this->data));
    }
}
