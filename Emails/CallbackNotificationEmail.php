<?php

namespace Modules\PaymentApi\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class CallbackNotificationEmail.
 *
 * @package Modules\PaymentApi\Emails
 */
class CallbackNotificationEmail extends Mailable
{
    use Queueable;
    use SerializesModels;

    public $data;
    
    /**
     * Create a new message instance.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if (isset($this->data['subject']) && null != $this->data['subject']) {
            $subject = $this->data['subject'];
        } else {
            $subject = 'Notification';
        }

        return $this
            ->subject($subject)
            ->markdown('paymentapi::Mails.callback');
    }
}
