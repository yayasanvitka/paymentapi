<?php

namespace Modules\PaymentApi\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class PaymentApi.
 *
 * @package Modules\PaymentApi\Facades
 */
class PaymentApi extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'PaymentApi';
    }
}
