<?php

namespace Modules\PaymentApi;

use Illuminate\Support\Collection;
use Modules\Paymentapi\Transactions\CreateBilling\CreateBilling;
use Modules\Paymentapi\Transactions\InquiryBilling\InquiryBilling;
use Modules\Paymentapi\Transactions\UpdateBilling\UpdateBilling;

/**
 * Class PaymentApi.
 *
 * @package Modules\PaymentApi
 */
class PaymentApi
{
    /**
     * @var CreateBilling
     */
    protected $create;
    /**
     * @var UpdateBilling
     */
    protected $update;
    /**
     * @var InquiryBilling
     */
    protected $inquiry;

    /**
     * PaymentApi constructor.
     */
    public function __construct()
    {
        $this->create = new CreateBilling();
        $this->update = new UpdateBilling();
        $this->inquiry = new InquiryBilling();
    }
    
    /**
     * @param array $data
     * @return Collection|null
     */
    public function createBilling(array $data)
    {
        return $this->create->prepare($data)->validate()->send();
    }
    
    /**
     * @param array $data
     * @return Collection|null
     */
    public function inquiryBilling(array $data)
    {
        return $this->inquiry->prepare($data)->validate()->send();
    }
    
    /**
     * @param array $data
     * @return Collection|null
     */
    public function updateBilling(array $data)
    {
        return $this->update->prepare($data)->validate()->send();
    }
    
    /**
     * @param array $data
     * @return Collection|null
     */
    public function forceExpire(array $data)
    {
        return $this->update->expiring($data)->validate()->send();
    }
}
